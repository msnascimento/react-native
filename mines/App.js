
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Alert
} from 'react-native';

import params from './src/params'
import { createMinedBoard, cloneBoard, openField, hadExplosion, showMines, wonGame, invertFlag, flagsUsed } from './src/functions'
import MineField from './src/components/MineField'
import Header from './src/components/Header'
import LevelSelection from './src/components/LevelSelection';



export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = this.createState()
  }

  minesAmount = () => {
    const cols = params.getColumnsAmount()
    const rows = params.getRowsAmount()
    return Math.ceil(cols * rows * params.difficultLevel)
  }

  createState = () => {
    const cols = params.getColumnsAmount()
    const rows = params.getRowsAmount()
    return {
      board: createMinedBoard(rows, cols, this.minesAmount()),
      lost: false,
      won: false,
      showLevelSelection: false,
    }
  }

  onOpenField = (row, column) => {

    const board = cloneBoard(this.state.board)
    openField(board, row, column)
    const lost = hadExplosion(board)
    const won = wonGame(board)

    if (lost) {
      showMines(board)
      Alert.alert('Perdeuuuuuu', 'que burro!')
    }

    if (won) {
      Alert.alert('Parabens', 'voce venceu!')
    }

    this.setState({ board, lost, won })
  }

  onSelectField = (row, column) => {

    const board = cloneBoard(this.state.board)
    invertFlag(board, row, column)

    const won = wonGame(board)

    if (won) {
      Alert.alert('Parabens', 'voce venceu!')
    }

    this.setState({ board, won })
  }

  onLevelSelect = level => {
    params.difficultLevel = level
    this.setState(this.createState())
  }

  render() {
    return (
      <View style={styles.sectionContainer}>
        <LevelSelection
          isVisible={this.state.showLevelSelection}
          onLevelSelect={this.onLevelSelect}
          onCancel={() => this.setState({ showLevelSelection: false })} />
        <Header flagsLeft={this.minesAmount() - flagsUsed(this.state.board)}
          onNewGame={() => this.setState(this.createState())}
          onFlagPress={() => this.setState({ showLevelSelection: true })} />
        <View style={styles.board}>
          <MineField board={this.state.board}
            onOpenField={this.onOpenField}
            onSelectField={this.onSelectField}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  board: {
    alignItems: 'center',
    backgroundColor: '#aaa'
  }
});
