
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Alert,
  Modal,
  TouchableOpacity
} from 'react-native';



export default props => {
  return (
    <TouchableOpacity
      style={[styles.button, styles.bgEasy]}
      onPress={() => props.onLevelSelect(props.value)}>
      <Text style={styles.buttonLabel}>{props.label}</Text>
    </TouchableOpacity>
  )
}




const styles = StyleSheet.create({
  frame: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)'
  }, container: {
    backgroundColor: '#eee',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  button: {
    marginTop: 10,
    padding: 5,
  },
  buttonLabel: {
    fontSize: 20,
    color: '#eee'
  },
  bgEasy: { backgroundColor: '#49b65d' },
  bgNormal: { backgroundColor: '#2765f7' },
  bgHard: { backgroundColor: '#F26337' },
})