import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import Field from './Field'

export default props => {
  const rows = props.board.map((row, r) => {
    const columns = row.map((field, c) => {
      return <Field {...field} key={c}
        onOpen={() => props.onOpenField(r, c)}
        onSelect={() => props.onSelectField(r, c)} />
    })
    return <View style={{ flexDirection: 'row' }} keys={r}>{columns}</View>
  })
  return <View style={styles.sectionContainer}>{rows}</View>
}


const styles = StyleSheet.create({
  sectionContainer: {
    backgroundColor: '#eee'
  }
})