
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Alert,
  Modal,
  TouchableOpacity
} from 'react-native';
import Level from './Level'


export default props => {

  return (
    <Modal onRquestClose={props.onCancel}
      visible={props.isVisible}
      animationType='slide'
      transparent={true}>
      <View style={styles.frame}>
        <View style={styles.container}>
          <Text style={styles.title}>Selecione o nível</Text>
          <Level label={'Fácil'} value={0.1} onLevelSelect={props.onLevelSelect} />
          <Level label={'Médio'} value={0.2} onLevelSelect={props.onLevelSelect} />
          <Level label={'Difícil'} value={0.3} onLevelSelect={props.onLevelSelect} />
        </View>
      </View>
    </Modal>
  )
}


const styles = StyleSheet.create({
  frame: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)'
  }, container: {
    backgroundColor: '#eee',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  button: {
    marginTop: 10,
    padding: 5,
  },
  buttonLabel: {
    fontSize: 20,
    color: '#eee'
  },
  bgEasy: { backgroundColor: '#49b65d' },
  bgNormal: { backgroundColor: '#2765f7' },
  bgHard: { backgroundColor: '#F26337' },
})