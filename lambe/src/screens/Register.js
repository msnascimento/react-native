import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Platform,
  Image,
  TextInput,
  TouchableWithoutFeedback as TWF,
  Alert,
  FlatList,
  ScrollView,
  ScrollViewBase,
  TouchableOpacityBase
} from 'react-native'



class Register extends React.Component {

  state = { name: '', email: '', password: '' }

  render() {

    return (
      <View style={styles.container}>
        <TextInput style={styles.input}
          placeholder='Nome'
          autoFocus={true}
          onChangeText={nome => this.setState({ nome })} />
        <TextInput style={styles.input}
          placeholder='Email'
          keyboardType='email-address'
          value={this.state.email}
          onChangeText={email => this.setState({ email })} />
        <TextInput style={styles.input}
          placeholder='Senha'
          value={this.state.password}
          onChangeText={password => this.setState({ password })}
          secureTextEntry={true} />

        <TouchableOpacity onPress={() => { }} style={styles.button}>
          <Text style={styles.buttonText}>Salvar</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    marginTop: 30,
    padding: 10,
    backgroundColor: '#4286f4'
  },
  buttonText: {
    fontSize: 20,
    color: '#fff'
  },
  input: {
    marginTop: 20,
    width: '90%',
    height: 40,
    backgroundColor: '#eee',
    borderRadius: 1,
    borderColor: '#3eee',
    paddingLeft: 15
  }
})

export default Register


