import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Platform,
  Image,
  TextInput,
  TouchableWithoutFeedback as TWF,
  Alert,
  FlatList,
  ScrollView,
  ScrollViewBase,
  TouchableOpacityBase
} from 'react-native'


import { connect } from 'react-redux'
import { login } from '../store/actions/user'

class Login extends React.Component {
  state = {
    name: 'Temporario',
    email: '',
    senha: ''
  }

  login = () => {
    this.props.onLogin({ ...this.state })
    this.props.navigation.navigate('Profile')
  }

  render() {

    return (
      <View style={styles.container}>
        <TextInput style={styles.input}
          placeholder='Email'
          autoFocus={true}
          keyboardType='email-address'
          value={this.state.email}
          onChangeText={email => this.setState({ email })}
        />

        <TextInput style={styles.input}
          placeholder='Senha'
          autoFocus={true}
          keyboardType='email-address'
          value={this.state.password}
          onChangeText={password => this.setState({ password })}
          secureTextEntry={true} />

        <TouchableOpacity onPress={this.login} style={styles.button}>
          <Text style={styles.buttonText}>Login</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {
          this.props.navigation.navigate('Register')
        }} style={styles.button}>
          <Text style={styles.buttonText}>Criar nova Conta</Text>
        </TouchableOpacity>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    marginTop: 30,
    padding: 10,
    backgroundColor: '#4286f4'
  },
  buttonText: {
    fontSize: 20,
    color: '#fff'
  },
  input: {
    marginTop: 20,
    width: '90%',
    height: 40,
    backgroundColor: '#eee',
    borderRadius: 1,
    borderColor: '#3eee'
  }
})


const mapDispatchToProps = dispatch => {
  return {
    onLogin: user => dispatch(login(user))
  }
}

export default connect(null, mapDispatchToProps)(Login)
