import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Platform,
  Image,
  TextInput,
  TouchableWithoutFeedback as TWF,
  Alert,
  FlatList,
  ScrollView,
  ScrollViewBase,
  TouchableOpacityBase
} from 'react-native'
import ImagePicker from 'react-native-image-picker'
import { connect, useStore } from 'react-redux'
import { addPost } from '../store/actions/post'

noUser = 'Voce precisa esta logado para adicionar imagens!'

class AddPhoto extends React.Component {

  state = {
    image: null,
    comment: '',
  }

  PickImage = () => {
    if (!this.props.name) {
      Alert.alert('Falha!', noUser)
      return
    }

    ImagePicker.showImagePicker({
      title: 'Escolha a imagem',
      maxHeight: 600,
      maxWidth: 600,
    },
      res => {
        if (!res.didCancel) {
          this.setState({ image: { uri: res.uri, base64: res.data } })
        }
      })
  }


  save = async () => {
    if (!this.props.name) {
      Alert.alert('Falha!', noUser)
      return
    }

    this.props.onAddPost({
      id: Math.random(),
      nickname: this.props.name,
      email: this.props.email,
      image: this.state.image,
      comments: [{
        nickname: this.props.name,
        comment: this.state.comment
      }]
    })

    this.setState({ image: null, comment: '' })
    this.props.navigation.navigate('Feed')
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Text style={styles.title}>Compartilhe uma imagem</Text>
          <View style={styles.imgContainer}>
            <Image source={this.state.image} style={styles.image} />
          </View>
          <TouchableOpacity onPress={this.PickImage} style={styles.button}>
            <Text style={styles.buttomText}>Escolha a foto</Text>
          </TouchableOpacity>
          <TextInput
            placeholder='Algum comentario para a foto?'
            style={styles.input}
            editable={this.props.name != null}
            value={this.state.comment}
            onChangeText={comment => this.setState({ comment })}
          />
          <TouchableOpacity onPress={this.save} style={styles.button}>
            <Text style={styles.buttomText}>Salvar</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    )
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  }, title: {
    fontSize: 20,
    marginTop: Platform.OS === 'ios' ? 30 : 10,
    fontWeight: 'bold'
  },
  imageContainer: {
    width: '90%',
    height: Dimensions.get('window').width * 3 / 4,
    backgroundColor: '#eee',
    marginTop: 10,
  },
  image: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').width * 3 / 4,
    resizeMode: 'center',
  },
  button: {
    marginTop: 30,
    padding: 10,
    backgroundColor: '#4286f4'
  },
  buttonText: {
    fontSize: 20,
    color: '#fff'
  },
  input: {
    marginTop: 20,
    width: '90%'
  }
})

const mapStateToProps = ({ user }) => {
  return {
    email: user.email,
    name: user.name
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAddPost: post => dispatch(addPost(post))
  }
}



export default connect(mapStateToProps, mapDispatchToProps)(AddPhoto)