import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableHighlight,
  Platform,
  Image
} from 'react-native'

import Autor from './Autor'
import Comentario from './Comentario'
import AddComment from './AddComment'

import { connect } from 'react-redux'


class Post extends React.Component {
  render() {
    const addComment = this.props.name ?
      <AddComment postId={this.props.id} />
      : null
    return (
      <View style={styles.container} >
        <Image source={this.props.image} style={styles.image} />
        <Autor email={this.props.email} nickname={this.props.nickname} />
        <Comentario comments={this.props.comments} />
        {addComment}
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').width * 3 / 4,
    resizeMode: 'contain'
  }

})


const mapStateToProps = ({ user }) => {
  return {
    name: user.name
  }
}

export default connect(mapStateToProps)(Post)