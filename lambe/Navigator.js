import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableHighlight,
  Platform,
  Image
} from 'react-native'

import { createBottomTabNavigator, createSwitchNavigator, createStackNavigator } from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome'

import Feed from './src/screens/Feed'
import AddPhoto from './src/screens/AddPhoto'
import Profile from './src/screens/Profile'
import Login from './src/screens/Login'
import Register from './src/screens/Register'

const authRouter = createStackNavigator({
  Login: { screen: Login, navigationOptions: { title: 'Login' } },
  Register: { screen: Register, navigationOptions: { title: 'Register' } }
})


const LoginOrProfileRouter = createSwitchNavigator({
  Profile: Profile,
  Auth: authRouter

}, {
  initialRouteName: 'Profile'
})

const MenuRoutes = {

  Feed: {
    name: 'Feed',
    screen: Feed,
    navigationOptions: {
      title: 'Feed',
      tabBarIcon: ({ tintColor }) =>
        <Icon name='home' size={30} color={tintColor} />
    }
  },
  Add: {
    name: 'AddPhoto',
    screen: AddPhoto,
    navigationOptions: {
      title: 'Add Picture',
      tabBarIcon: ({ tintColor }) =>
        <Icon name='camera' size={30} color={tintColor} />
    }
  },
  Profile: {
    name: 'Profile',
    screen: LoginOrProfileRouter,
    navigationOptions: {
      title: 'Profile',
      tabBarIcon: ({ tintColor }) =>
        <Icon name='user' size={30} color={tintColor} />
    }
  },
}


const MenuConfig = {
  initialRouteName: 'Feed',
  tabBarOptions: { showLabel: false }
}

const MenuNavigator = createBottomTabNavigator(MenuRoutes, MenuConfig)

export default MenuNavigator
